package com.shanshan.MovieCharacter.controllers;

import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/movies")
public class MovieController {
    @Autowired
    private MovieService movieService;

    // CRUD
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){
        return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getSpecificMovie(@PathVariable Long id){
        return movieService.getSpecificMovie(id);
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        return movieService.addMovie(movie);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        return movieService.updateMovie(id, movie);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSpecificMovie(@PathVariable Long id){
        return movieService.deleteSpecificMovie(id);
    }

    // Update characters in a specific movie by taking in an array of character ids
    @PutMapping("/{id}/characters")
    public ResponseEntity<Movie> updateMovieCharacters(@PathVariable Long id, @RequestBody Long[] characterIds){
        return movieService.updateMovieCharacters(id, characterIds);
    }
}
