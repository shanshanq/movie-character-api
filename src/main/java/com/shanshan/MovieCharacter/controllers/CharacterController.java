package com.shanshan.MovieCharacter.controllers;

import com.shanshan.MovieCharacter.models.Character;
import com.shanshan.MovieCharacter.services.CharacterService;
import com.shanshan.MovieCharacter.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/characters")
public class CharacterController {
    @Autowired
    private CharacterService characterService;

    // CRUD
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters(){
        return characterService.getAllCharacters();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Character> getSpecificCharacter(@PathVariable Long id){
        return characterService.getSpecificCharacter(id);
    }

    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        return characterService.addCharacter(character);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        return characterService.updateCharacter(id, character);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSpecificCharacter(@PathVariable Long id){
        return characterService.deleteSpecificCharacter(id);
    }

}
