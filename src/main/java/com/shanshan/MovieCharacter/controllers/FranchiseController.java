package com.shanshan.MovieCharacter.controllers;

import com.shanshan.MovieCharacter.models.Character;
import com.shanshan.MovieCharacter.models.Franchise;
import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.services.FranchiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/franchises")
public class FranchiseController {
    @Autowired
    private FranchiseService franchiseService;

    // CRUD
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        return franchiseService.getAllFranchises();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getSpecificFranchise(@PathVariable Long id){
        return franchiseService.getSpecificFranchise(id);
    }
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        return franchiseService.addFranchise(franchise);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        return franchiseService.updateFranchise(id, franchise);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSpecificFranchise(@PathVariable Long id){
        return franchiseService.deleteSpecificFranchise(id);
    }

    // Update movies in a specific franchise by taking in an array of movie ids
    @PutMapping("/{id}/movies")
    public ResponseEntity<Franchise> updateFranchiseMovies(@PathVariable Long id, @RequestBody Long[] movieIds){
        return franchiseService.updateFranchiseMovies(id, movieIds);
    }

}
