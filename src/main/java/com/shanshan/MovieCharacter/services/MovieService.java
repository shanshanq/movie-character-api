package com.shanshan.MovieCharacter.services;

import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.repositories.CharacterRepository;
import com.shanshan.MovieCharacter.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import com.shanshan.MovieCharacter.models.Character;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class MovieService {
    @Autowired
    MovieRepository movieRepository;

    @Autowired
    CharacterRepository characterRepository;

    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies, status);
    }

    public ResponseEntity<Movie> getSpecificMovie(Long id) {
        HttpStatus status;
        Movie movie = new Movie();
        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movie, status);
        }
        movie = movieRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(movie, status);
    }

    public ResponseEntity<Movie> addMovie(Movie movie) {
        Movie returnMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnMovie, status);
    }

    public ResponseEntity<Movie> updateMovie(Long id, Movie movie) {
        HttpStatus status;
        Movie returnMovie = new Movie();

        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    public ResponseEntity<?> deleteSpecificMovie(Long id) {
        movieRepository.delete(movieRepository.getById(id));
        HttpStatus status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(status);
    }

    public ResponseEntity<Movie> updateMovieCharacters(Long id, Long[] characterIds) {
        HttpStatus status;
        Set<Character> characters = new HashSet<>();
        Movie returnMovie = new Movie();

        for (Long characterId : characterIds) {
            characters.add(characterRepository.findById(characterId).get());
        }
        Movie movie = movieRepository.findById(id).get();
        movie.setCharacters(characters);
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }
}
