package com.shanshan.MovieCharacter.services;

import com.shanshan.MovieCharacter.models.Character;
import com.shanshan.MovieCharacter.models.Franchise;
import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.repositories.FranchiseRepository;
import com.shanshan.MovieCharacter.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class FranchiseService {

    @Autowired
    FranchiseRepository franchiseRepository;

    @Autowired
    MovieRepository movieRepository;

    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(franchises, status);
    }

    public ResponseEntity<Franchise> getSpecificFranchise(Long id) {
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(franchise, status);
        }
        franchise = franchiseRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise, status);
    }

    public ResponseEntity<Franchise> addFranchise(Franchise franchise) {
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise, status);
    }

    public ResponseEntity<Franchise> updateFranchise(Long id, Franchise franchise) {
        HttpStatus status;
        Franchise returnFranchise = new Franchise();

        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise,status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise,status);
    }

    public ResponseEntity<?> deleteSpecificFranchise(Long id) {
        franchiseRepository.delete(franchiseRepository.getById(id));
        HttpStatus status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(status);
    }

    public ResponseEntity<Franchise> updateFranchiseMovies(Long id, Long[] movieIds) {
        HttpStatus status;
        Set<Movie> movies = new HashSet<>();
        Franchise returnFranchise = new Franchise();

        for(Long movieId: movieIds){
            movies.add(movieRepository.findById(movieId).get());
        }
        Franchise franchise = franchiseRepository.findById(id).get();
        franchise.setMovies(movies);
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise, status);
    }

}
