package com.shanshan.MovieCharacter.services;

import com.shanshan.MovieCharacter.models.Franchise;
import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.models.Character;
import com.shanshan.MovieCharacter.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CharacterService {
    @Autowired
    private CharacterRepository characterRepository;

    public ResponseEntity<List<Character>> getAllCharacters(){
        List<Character> characters = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(characters, status);
    }

    public ResponseEntity<Character> getSpecificCharacter(Long id) {
        HttpStatus status;
        Character character = new Character();
        if(!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(character, status);
        }
        character = characterRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(character, status);
    }

    public ResponseEntity<Character> addCharacter(Character character) {
        Character returnCharacter = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCharacter, status);
    }

    public ResponseEntity<Character> updateCharacter(Long id, Character character) {
        HttpStatus status;
        Character returnCharacter = new Character();

        if (!id.equals(character.getId())) {
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnCharacter,status);
        }
        returnCharacter = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnCharacter,status);
    }


    public ResponseEntity<?> deleteSpecificCharacter(Long id) {
        characterRepository.delete(characterRepository.getById(id));
        HttpStatus status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(status);
    }
}
