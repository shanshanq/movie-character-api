package com.shanshan.MovieCharacter.repositories;

import com.shanshan.MovieCharacter.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}