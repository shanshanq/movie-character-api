package com.shanshan.MovieCharacter.repositories;

import com.shanshan.MovieCharacter.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}