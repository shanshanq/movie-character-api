package com.shanshan.MovieCharacter.repositories;

import com.shanshan.MovieCharacter.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface CharacterRepository extends JpaRepository<Character, Long> {
}