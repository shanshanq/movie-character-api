package com.shanshan.MovieCharacter;

import com.shanshan.MovieCharacter.models.Franchise;
import com.shanshan.MovieCharacter.models.Movie;
import com.shanshan.MovieCharacter.models.Character;
import com.shanshan.MovieCharacter.repositories.CharacterRepository;
import com.shanshan.MovieCharacter.repositories.FranchiseRepository;
import com.shanshan.MovieCharacter.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class AppRunner implements ApplicationRunner {
    @Autowired
    MovieRepository movieRepo;

    @Autowired
    CharacterRepository characterRepo;

    @Autowired
    FranchiseRepository franchiseRepo;


    @Override
    public void run(ApplicationArguments args) throws Exception {
        // Data Seeding

        // Franchises
        ArrayList<Franchise> franchises = new ArrayList<>();
        Franchise mcu = new Franchise("Marvel Cinematic Universe","MCU the best");
        Franchise batmanF = new Franchise("Batman","Wow");
        franchises.add(mcu);
        franchises.add(batmanF);
        franchiseRepo.saveAll(franchises);

        // Characters
        ArrayList<Character> characters = new ArrayList<>();
        Character bale = new Character("Christian Bale", "Bale", "Male", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        Character hemsworth = new Character("Chris Hemsworth","Hemsworth","Male","https://www.imdb.com/name/nm1165110/mediaviewer/rm3936529408/");
        Character johansson = new Character("Scarlett Johansson","Johansson", "Female","https://www.imdb.com/name/nm0424060/mediaviewer/rm1916122112/");
        characters.add(bale);
        characters.add(hemsworth);
        characters.add(johansson);
        characterRepo.saveAll(characters);

        // Movies
		ArrayList<Movie> movies = new ArrayList<>();
		Movie thor = new Movie("Thor","Action, Adventure, Fantasy","2011","Kenneth Branagh","https://www.imdb.com/title/tt0800369/mediaviewer/rm3272546304/","https://www.imdb.com/video/vi1431476761?playlistId=tt0800369");
		Movie batman = new Movie("Batman Begins","Action, Adventure","2005","Christopher Nolan","https://www.imdb.com/title/tt0372784/mediaviewer/rm2827249920/","https://www.imdb.com/video/vi362988313?playlistId=tt0372784");
		Movie avengers = new Movie("The Avengers","Action, Adventure, Sci-fi","2012","Joss Whedon","https://www.imdb.com/title/tt5311514/mediaviewer/rm3216783616/","https://www.imdb.com/video/vi1705097753?playlistId=tt5311514");


		thor.setCharacters(Set.of(hemsworth));
        batman.setCharacters(Set.of(bale));
        avengers.setCharacters(Set.of(johansson,hemsworth));

        thor.setFranchise(mcu);
        batman.setFranchise(batmanF);
        avengers.setFranchise(mcu);

		movies.add(thor);
		movies.add(batman);
		movies.add(avengers);
		movieRepo.saveAll(movies);
    }
}
